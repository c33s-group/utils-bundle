<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Definition Url: https://ogp.me/.
 */
class OpenGraphExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('og_variables_add', [$this, 'addVariables'], ['needs_context' => true]),
        ];
    }

    /**
     * Used for _opengraph.html.twig. called in base.html.twig to add default variables.
     *
     * @param mixed $context
     */
    public function addVariables(&$context, array $additionalObVariables): void
    {
        $currentOgVariables = $context['og_variables'] ?? [];
        $context['og_variables'] = array_merge($currentOgVariables, $additionalObVariables);
    }
}
