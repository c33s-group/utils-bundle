<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class ValidFileExtExtension extends AbstractExtension
{
    private const DEFAULT_EXTENSIONS = ['jpg', 'png', 'jpeg'];
    /**
     * @var string[]
     */
    private $validFileExtensions;

    public function __construct(?array $validFileExtensions = null)
    {
        if (null === $validFileExtensions) {
            $validFileExtensions = self::DEFAULT_EXTENSIONS;
        } else {
            $validFileExtensions = array_map('strtolower', $validFileExtensions);
        }
        $this->validFileExtensions = $validFileExtensions;
    }

    public function getTests(): array
    {
        return [
            new TwigTest('valid_extension', [$this, 'checkFileExtensions']),
        ];
    }

    /**
     * Checks if an image file name is empty and if it has an image specific file extension.
     */
    public function checkFileExtensions(?string $fileName, ?array $validFileExtensions = null): bool
    {
        if (null === $validFileExtensions) {
            $validFileExtensions = $this->validFileExtensions;
        } else {
            $validFileExtensions = array_map('strtolower', $validFileExtensions);
        }

        if (null === $fileName) {
            return false;
        }
        $fileName = trim($fileName);
        if ('' === $fileName) {
            return false;
        }

        $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        if (in_array($extension, $validFileExtensions, true)) {
            return true;
        }

        return false;
    }
}
