<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use C33s\Utils\ProxyTools;
use Cocur\Slugify\SlugifyInterface;
use Symfony\Bridge\Twig\Extension\AssetExtension;
use Symfony\Component\Filesystem\Filesystem;
//use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Mime\MimeTypesInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageAssetExtension extends AbstractExtension
{
    /**
     * @var AssetExtension
     */
    private $assetExtension;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var string
     */
    private $cacheDirSuffix;

    /**
     * Var SluggerInterface # can be used again if upgrading to php ^7.2.
     *
     * @var SlugifyInterface
     */
    private $slugger;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var MimeTypesInterface
     */
    private $mimeTypes;

    /**
     * @var Filesystem
     */
    private $filesystem;

    const DEFAULT_CACHE_DIR_SUFFIX = 'image-asset';

    public function __construct(
        AssetExtension $assetExtension,
        string $cacheDir,
        SlugifyInterface $slugger,
        Filesystem $filesystem,
        MimeTypesInterface $mimeTypes,
        string $locale = 'en',
        string $imageAssetCacheDirSuffix = self::DEFAULT_CACHE_DIR_SUFFIX
    ) {
        $this->assetExtension = $assetExtension;
        $this->cacheDir = $cacheDir;
        $this->cacheDirSuffix = $imageAssetCacheDirSuffix;
        $this->slugger = $slugger;
        $this->locale = $locale;
        $this->mimeTypes = $mimeTypes;
        $this->filesystem = $filesystem;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('asset_image', [$this, 'assetImage']),
        ];
    }

    private function slug(string $string): string
    {
        return $this->slugger->slugify($string);
//        return $this->slugger->slug($string, '-', $this->locale)->toString();
    }

    public function assetImage(string $path, string $packageName = null): string
    {
        if (false !== filter_var($path, FILTER_VALIDATE_URL)) {
            $urlInfo = parse_url($path);
            if (!is_array($urlInfo)) {
                return $this->assetExtension->getAssetUrl($path, $packageName);
            }
            if (!array_key_exists('path', $urlInfo)) {
                return $this->assetExtension->getAssetUrl($path, $packageName);
            }
            $urlPath = $urlInfo['path'];
            $pathInfo = pathinfo($urlPath);
            $host = 'unknown_host';
            if (array_key_exists('host', $urlInfo)) {
                $host = $urlInfo['host'];
            }

            //fallback to jpg even if it's not the right ext because of performance reasons
            $extension = 'jpg';
            $query = '';
            if (array_key_exists('extension', $pathInfo)) {
                $extension = $pathInfo['extension'];
            }
            if (array_key_exists('query', $urlInfo)) {
                $query = $urlInfo['query'];
            }

            $toSlug = $pathInfo['filename'].'-'.$query;
            $localName = $this->slug($toSlug).'.'.$extension;
            $relativeLocalDir = "$host{$pathInfo['dirname']}";
            $absoluteLocalDir = "{$this->cacheDir}/{$this->cacheDirSuffix}/{$relativeLocalDir}";
            $absoluteLocalPath = "{$absoluteLocalDir}/{$localName}";
            $relativeLocalPath = "{$relativeLocalDir}/{$localName}";

            if (!$this->filesystem->exists($absoluteLocalPath)) {
                $this->filesystem->mkdir($absoluteLocalDir);
                file_put_contents($absoluteLocalPath, ProxyTools::proxyAwareFileGetContents($path));
            }

            $path = $relativeLocalPath;
        }

        return $this->assetExtension->getAssetUrl($path, $packageName);
    }
}
//            $mimeType = $this->mimeTypes->guessMimeType('/some/path/to/image.gif');
//            $exts = $mimeTypes->getExtensions('image/jpeg');
//            $exts = ['jpeg', 'jpg', 'jpe']
//    new TwigFunction('asset_image', [$this, 'assetImage'], ['needs_context' => true, 'needs_environment' => true]),
//    public function assetImage($environment, &$context, $additionalObVariables): string
