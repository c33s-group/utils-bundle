<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AddClassesExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('add_classes', [$this, 'addClasses']),
        ];
    }

    /**
     * Add classes to (html) attributes. First implemented to be used with knpmenu.
     *
     * <code>
     *      item.attributes|add_classes(['navbar-nav', 'me-auto'])
     * </code>
     *
     * @param string|string[] $newClasses
     */
    public function addClasses(array $existingAttributes, $newClasses): array
    {
        if (is_string($newClasses)) {
            $newClasses = [$newClasses];
        }

        return $this->addClassesToArray($existingAttributes, $newClasses);
    }

    private function addClassesToArray(array $existingAttributes, array $newClasses): array
    {
        $allClasses = $newClasses;
        if (array_key_exists('class', $existingAttributes)) {
            $existingClasses = explode(' ', $existingAttributes['class']);
            $allClasses = array_merge($existingClasses, $newClasses);
            unset($existingAttributes['class']);
        }
        // trim whitespaces
        $allClasses = array_map('trim', $allClasses);
        // remove empty
        $allClasses = array_filter($allClasses);
        // make unique
        $allClasses = array_unique($allClasses);

        return array_merge($existingAttributes, ['class' => implode(' ', $allClasses)]);
    }
}
