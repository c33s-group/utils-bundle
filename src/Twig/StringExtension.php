<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use C33s\Utils\String\UnicodeString;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Only load the filters if symfony/string is not there.
 */
class StringExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        if (!class_exists('\Twig\Extra\String\StringExtension')) {
            return [
                new TwigFilter('u', [$this, 'createUnicodeString']),
            ];
        }

        return [];
    }

    public function createUnicodeString(?string $text): UnicodeString
    {
        return new UnicodeString($text ?? '');
    }
}
