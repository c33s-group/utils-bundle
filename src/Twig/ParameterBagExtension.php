<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ParameterBagExtension extends AbstractExtension
{
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_parameter', [$this, 'getParameter']),
        ];
    }

    /**
     * @return mixed|null
     */
    public function getParameter(string $name)
    {
        if ($this->parameterBag->has($name)) {
            return $this->parameterBag->get($name);
        }

        return null;
    }
}
