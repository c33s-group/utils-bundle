<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class C33sUtilsBundle extends Bundle
{
}
