# OpenGraph Helper
https://ogp.me/

## Quickstart

Add this to your `base.html.twig` and also define your core defaults there
`base.html.twig`:
```twig
<!DOCTYPE html prefix="{% block html_prefix %}{% endblock %}">
...
{% block open_graph %}
    {% set og_variables_defaults = {'title': block('title')} %}
    {% set og_variables_internal = og_variables_defaults|merge(og_variables|default({})) %}
    {% include '_opengraph.html.twig' with { 'og_variables': og_variables_internal } only %}
{% endblock %}
...
```
the title which can be fetched from the block must be set there, because it can't be fetched from the included file.

Add the `open_graph_logo` filter to `liip_imagine.yaml`
`config/liip_imagine.yaml`:
```yaml
liip_imagine:
   filter_sets:
        open_graph_logo:
            quality: 70
            filters:
                thumbnail: { size: [316, 316], mode: outbound, allow_upscale: true }
                strip: ~
```

## Override variables

there are the following variables (key names) which can be override from twig or
the controller:

```
og_variables['max_description_length']
og_variables['asset_version']
og_variables['type']
og_variables['site_name']
og_variables['domain']
og_variables['title']
og_variables['description']
og_variables['keywords']
og_variables['url']
og_variables['image_name']
og_variables['image_url']
```

### From Twig

Add this in your templates before the extend part to add or override variables
with `og_variables_add`:
```twig

{% do og_variables_add({'title': 'example title', 'extranew': 'bar'}) %}

{% extends 'base.html.twig' %}
...
```

### From Controller
`YourController.php`:
```php
return $this->render('example/index.html.twig', [
    'og_variables' => ['extra' => 'my extra foo'],
]);
```
