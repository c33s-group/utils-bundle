<?php

declare(strict_types=1);


namespace Tests\Unit;

use C33s\Bundle\UtilsBundle\Twig\ValidFileExtExtension;
use Codeception\Test\Unit;

/**
 * @group twig
 */
final class ValidFileExtensionTest extends Unit
{
    /**
     * Extensions are provided with the constructor
     *
     * @dataProvider provideFixtures
     */
    public function testCheckFileExtensionsWithConstructorProvidedExtensions(string $filename, ?array $extensions, bool $expected): void
    {
        $extension = new ValidFileExtExtension($extensions);
        $actual = $extension->checkFileExtensions($filename);

        self::assertEquals($expected, $actual);
    }

    /**
     * Extensions are provided with the function call
     *
     * @dataProvider provideFixtures
     */
    public function testCheckFileExtensionsWithFunctionProvidedExtensions(string $filename, ?array $extensions, bool $expected): void
    {
        $extension = new ValidFileExtExtension(null);
        $actual = $extension->checkFileExtensions($filename, $extensions);

        self::assertEquals($expected, $actual);
    }

    public function provideFixtures(): iterable
    {
        yield 'empty filename and empty extensions' => [ 'filename' => '', 'extensions' => [], 'expected' => false];
        yield 'jpg filename and empty extensions' => [ 'filename' => 'example.jpg', 'extensions' => [], 'expected' => false];
        yield 'jpg filename and default extensions' => [ 'filename' => 'example.jpg', 'extensions' => null, 'expected' => true];
        yield 'jpg filename and jpg extensions' => [ 'filename' => 'example.jpg', 'extensions' => ['jpg'], 'expected' => true];
        yield 'filename with space' => [ 'filename' => 'exa mple.jpg', 'extensions' => ['jpg'], 'expected' => true];
        yield 'filename with umlauts' => [ 'filename' => 'exämple.jpg', 'extensions' => ['jpg'], 'expected' => true];
        yield 'filename amd multiple extensions' => [ 'filename' => 'example.png', 'extensions' => ['jpg', 'png'], 'expected' => true];
        yield 'jpg filename and uppercase jpg extensions' => [ 'filename' => 'example.jpg', 'extensions' => ['JPG'], 'expected' => true];
    }
}
