<?php

namespace Tests\Unit\App\Namer;

use C33s\Bundle\UtilsBundle\Helper\TemplateNameBuilder;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use \InvalidArgumentException;
use C33s\Bundle\UtilsBundle\Helper\RequestStackHelper;

/**
 * @group twig
 *
 * Class TemplateNameBuilderTest
 */
class TemplateNameBuilderTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @test
     * @dataProvider provideValidNameMap
     *
     * @param $templatePath
     * @param $expectedTemplatePath
     * @param $isMasterRequest
     * @param $extension
     */
    public function templateNameBuilderTest(string $templatePath, string $expectedTemplatePath, bool $isMasterRequest)
    {
        $namer = new TemplateNameBuilder($this->getRequestStackHelper($isMasterRequest));
        $actual = $namer->build($templatePath);
        $this->assertEquals($expectedTemplatePath, $actual);
    }

    /**
     * @test
     * @dataProvider provideValidNameMapWithSuffix
     *
     * @param $templatePath
     * @param $expectedTemplatePath
     * @param $isMasterRequest
     * @param $extension
     */
    public function templateNameBuilderWithSuffixTest(string $templatePath, string $expectedTemplatePath, bool $isMasterRequest, string $suffix)
    {
        $namer = new TemplateNameBuilder($this->getRequestStackHelper($isMasterRequest));
        $actual = $namer->build($templatePath, $suffix);
        $this->assertEquals($expectedTemplatePath, $actual);
    }

    /**
     * @test
     * @dataProvider provideValidNameMapWithExtension
     *
     * @param $templatePath
     * @param $expectedTemplatePath
     * @param $isMasterRequest
     * @param $extension
     */
    public function templateNameBuilderWithExtensionTest(string $templatePath, string $expectedTemplatePath, bool $isMasterRequest, string $extension)
    {
        $namer = new TemplateNameBuilder($this->getRequestStackHelper($isMasterRequest));
        $actual = $namer->build($templatePath, '_embedded', $extension);
        $this->assertEquals($expectedTemplatePath, $actual);
    }

    /**
     * @test
     */
    public function templateNameBuilderWithWrongExtensionForMasterTest()
    {
        $namer = new TemplateNameBuilder($this->getMasterRequestStackHelper());
        $actual = $namer->build('search/index.test.example', '_embedded', '.test.different');
        $this->assertEquals('search/index.test.example', $actual);
    }

    /**
     * @test
     */
    public function templateNameBuilderWithWrongExtensionForSubTest()
    {
        $this->expectException(InvalidArgumentException::class);
        $namer = new TemplateNameBuilder($this->getSubRequestStackHelper());
        $namer->build('search/index.test.example', '_embedded', '.test.different');
    }

    public function provideValidNameMap()
    {
        return [
            ['search/index.html.twig', 'search/index.html.twig', true],
            ['search/index.html.twig', 'search/index_embedded.html.twig', false],
            ['search/foo/bar/index.html.twig', 'search/foo/bar/index.html.twig', true],
            ['search/foo/bar/index.html.twig', 'search/foo/bar/index_embedded.html.twig', false],
        ];
    }

    public function provideValidNameMapWithSuffix()
    {
        return [
            ['search/index.html.twig', 'search/index.html.twig', true, '_emb'],
            ['search/index.html.twig', 'search/index_emb.html.twig', false, '_emb'],
        ];
    }

    public function provideValidNameMapWithExtension()
    {
        return [
            ['search/index.test.example', 'search/index.test.example', true, '.test.example'],
            ['search/index.test.example', 'search/index_embedded.test.example', false, '.test.example'],
        ];
    }

    protected function getRequestStackHelper(bool $isMasterRequest)
    {
        if ($isMasterRequest) {
            return $this->getMasterRequestStackHelper();
        }

        return $this->getSubRequestStackHelper();
    }

    protected function getSubRequestStackHelper()
    {
        $stack = new RequestStack();
        $request1 = new Request();
        $request2 = new Request();
        $request3 = new Request();
        $stack->push($request1);
        $stack->push($request2);
        $stack->push($request3);
        $helper = new RequestStackHelper($stack);

        return $helper;
    }

    protected function getMasterRequestStackHelper()
    {
        $stack = new RequestStack();
        $request = new Request();
        $stack->push($request);
        $helper = new RequestStackHelper($stack);

        return $helper;
    }
}
