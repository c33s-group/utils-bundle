<?php

declare(strict_types=1);


namespace Tests\Unit;

use C33s\Bundle\UtilsBundle\Twig\AddClassesExtension;
use Codeception\Test\Unit;

/**
 * @group twig
 */
final class AddClassesExtensionTest extends Unit
{
    /**
     * @dataProvider provideFixtures
     */
    public function testAddClassesFilter(array $attributes, $classes, array $expected): void
    {
        $extension = new AddClassesExtension();
        $actual = $extension->addClasses($attributes, $classes);

        self::assertEquals($expected, $actual);
    }

    public function provideFixtures(): iterable
    {
        $defaultNewClass = 'added_class';
        $defaultNewClasses = ['added_class_one', 'added_class_two'];

        // one new class (string)
        yield 'add one class with existing attributes' => [
            'attributes' => ['src' => 'https://example.com', 'id' => 'example', 'class' => 'existing_class_one existing_class_two'],
            'classes' => $defaultNewClass,
            'expected' => ['src' => 'https://example.com', 'id' => 'example', 'class' => 'existing_class_one existing_class_two added_class'],
        ];
        yield 'add one class with class not defined yet' => [
            'attributes' => [],
            'classes' => $defaultNewClass,
            'expected' => ['class' => 'added_class'],
        ];
        yield 'add one class with existing empty class' => [
            'attributes' => ['class' => ''],
            'classes' => $defaultNewClass,
            'expected' => ['class' => 'added_class'],
        ];
        yield 'add one class with existing empty class with whitespaces' => [
            'attributes' => ['class' => '    '],
            'classes' => $defaultNewClass,
            'expected' => ['class' => 'added_class'],
        ];
        yield 'add one class with one existing class' => [
            'attributes' => ['class' => 'existing_class'],
            'classes' => $defaultNewClass,
            'expected' => ['class' => 'existing_class added_class'],
        ];
        yield 'add one class with two existing class' => [
            'attributes' => ['class' => 'existing_class_one existing_class_two'],
            'classes' => $defaultNewClass,
            'expected' => ['class' => 'existing_class_one existing_class_two added_class'],
        ];

        // multiple
        yield 'add two classes with class not defined yet' => [
            'attributes' => [],
            'classes' => $defaultNewClasses,
            'expected' => ['class' => 'added_class_one added_class_two'],
        ];
        yield 'add two classes with existing empty class' => [
            'attributes' => ['class' => ''],
            'classes' => $defaultNewClasses,
            'expected' => ['class' => 'added_class_one added_class_two'],
        ];
        yield 'add two classes with existing empty class with whitespaces' => [
            'attributes' => ['class' => '    '],
            'classes' => $defaultNewClasses,
            'expected' => ['class' => 'added_class_one added_class_two'],
        ];
        yield 'add two classes with one existing classes' => [
            'attributes' => ['class' => 'existing_class'],
            'classes' => $defaultNewClasses,
            'expected' => ['class' => 'existing_class added_class_one added_class_two'],
        ];
        yield 'add two classes with two existing classes' => [
            'attributes' => ['class' => 'existing_class_one existing_class_two'],
            'classes' => $defaultNewClasses,
            'expected' => ['class' => 'existing_class_one existing_class_two added_class_one added_class_two'],
        ];


        yield 'duplicate existing and new classes with lots of whitespaces' => [
            'attributes' => ['class' => '   existing_class_one    existing_class_two    existing_class_one   existing_class_one existing_class_three'],
            'classes' => ['added_class_one   ', '   added_class_two', 'added_class_two', '  added_class_two  '],
            'expected' => ['class' => 'existing_class_one existing_class_two existing_class_three added_class_one added_class_two'],
        ];
    }
}
