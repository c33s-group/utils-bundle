<?php

declare(strict_types=1);

// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// phpcs:disable PSR1.Files.SideEffects
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable Generic.Files.LineLength.TooLong
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass
// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
// phpcs:disable Generic.CodeAnalysis.EmptyStatement.DetectedCatch
// phpcs:disable Generic.Formatting.SpaceBeforeCast.NoSpace
define('C33S_SKIP_LOAD_DOT_ENV', true);
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
define('C33S_ROBO_DIR', '.robo');

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
(is_dir($roboDir) || mkdir($roboDir)) && chdir($roboDir);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === strpos(implode((array) $output), 'Nothing to install')) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use Consolidation\AnnotatedCommand\CommandData;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \C33s\Robo\BaseRoboFile
{
    use \C33s\Robo\C33sTasks;
    use \C33s\Robo\C33sExtraTasks;
    // use \C33s\Robo\DebugHooksTrait;

    /**
     * @hook pre-command
     */
    public function preCommand(CommandData $commandData)
    {
        $this->stopOnFail(true);
        $this->_prepareCiModules([
//            'codeception' => '2.4.1',
            'composer' => '2.0.7',
            'php-cs-fixer' => 'v2.16.1',
            'phpstan' => '0.9.2',
            'composer-unused' => '0.7.1',
            'composer-require-checker' => PHP_VERSION_ID < 70400 ? '2.0.0' : '3.2.0',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init()
    {
        $this->update();
        $this->reset();
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .robo/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Update the Project.
     */
    public function update()
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->_execPhp('php ./.robo/bin/composer.phar install --no-progress --no-suggest --prefer-dist --optimize-autoloader');
        } else {
            $this->_execPhp('php ./.robo/bin/composer.phar install');
        }
    }

    /**
     * Re-crates a fresh database (be careful all tables will be deleted).
     *
     * @param array $opts
     */
    public function reset($opts = ['limit' => 'none', 'bootstrap' => false])
    {
    }
}
